#!/usr/bin/env bash

set -e

CONFIG=Debug
if [[ "$#" -ge 1 ]] && [[ "$1" = "-r" ]]
then
	CONFIG=Release
	make -C examples -B config=release
else
	make -C examples -B
fi

for examp in build/${CONFIG}/examples/*
do
	BNAME="$(basename $examp)"
	EXPECTED_OUT="examples/tests/${BNAME%.*}.out"
	if [[ -f "${EXPECTED_OUT}" ]]
	then
		echo "--------------------------- Testing ${BNAME}"
		build/${CONFIG}/bin/blvrec $examp -e "${EXPECTED_OUT}"
	fi
done
